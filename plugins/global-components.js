import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import productListInfo from '~/components/product-list-info'
Vue.component('productListInfo', productListInfo)
